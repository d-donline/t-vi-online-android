package com.android.tuvionline;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.android.tuvionline.utils.DirectionUtils;

/**
 * Created by ABCD on 7/21/2016.
 */
public class PhongThuyActivity extends AppCompatActivity implements View.OnClickListener{
    private Button mBtnTrangPhuc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phongthuy);
        init();
    }
    private void init() {
        mBtnTrangPhuc = (Button) findViewById(R.id.btn_trang_phuc);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_trang_phuc:
                Intent mIntent1 = new Intent(this, TuViActivity.class);
                DirectionUtils.changeActivity(this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntent1);
                break;


            default:
                break;
        }
    }
}
