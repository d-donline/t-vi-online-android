package com.android.tuvionline;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.tuvionline.model.DataModel;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;

import com.squareup.picasso.Picasso;


/**
 * Created by ABCD on 7/21/2016.
 */
public class ChiTietActivity extends AppCompatActivity {

    private TextView tvReuslt , tvTitle;
    private DataModel mData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tongquan);
        Gson gson = new Gson();
        mData = gson.fromJson(getIntent().getStringExtra("POST_DATA"), DataModel.class);
        initActionBar();
        init();
    }

    private void init() {
        tvReuslt = (TextView) findViewById(R.id.tvResult);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        if(mData != null){
            tvTitle.setText(mData.title);
           // tvReuslt.setText(Html.fromHtml(mData.content));
            Spanned spanned = Html.fromHtml(mData.content,
                    new Html.ImageGetter() {
                        @Override
                        public Drawable getDrawable(String source) {
                            LevelListDrawable d = new LevelListDrawable();
                            Drawable empty = getResources().getDrawable(R.drawable.abc_btn_check_material);;
                            d.addLevel(0, 0, empty);
                            d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());
                            new ImageGetterAsyncTask(ChiTietActivity.this, source, d).execute(tvReuslt);

                            return d;
                        }
                    }, null);
            tvReuslt.setText(spanned);
        }

    }

    class ImageGetterAsyncTask extends AsyncTask<TextView, Void, Bitmap> {


        private LevelListDrawable levelListDrawable;
        private Context context;
        private String source;
        private TextView t;

        public ImageGetterAsyncTask(Context context, String source, LevelListDrawable levelListDrawable) {
            this.context = context;
            this.source = source;
            this.levelListDrawable = levelListDrawable;
        }

        @Override
        protected Bitmap doInBackground(TextView... params) {
            t = params[0];
            try {
                Log.d("qqqqq", "Downloading the image from: " + source);
                return Picasso.with(context).load(source).get();
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(final Bitmap bitmap) {
            try {
                Drawable d = new BitmapDrawable(getResources(), bitmap);
                Point size = new Point();
                getWindowManager().getDefaultDisplay().getSize(size);
                // Let's calculate the ratio according to the screen width in px
                int multiplier = size.x / bitmap.getWidth();
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                int extraPadding = 10;
                int contentWidth = t.getWidth()
                        - t.getPaddingLeft()
                        - t.getPaddingRight()
                        - extraPadding * 2;

                if (width > contentWidth) {
                    width = contentWidth;
                    height = height * width / bitmap.getWidth();
                } else {
                    extraPadding += (contentWidth - width) / 2;
                }
                levelListDrawable.addLevel(1, 1, d);
                // Set bounds width and height according to the bitmap resized size
                levelListDrawable.setBounds(extraPadding, 0, extraPadding
                        + width, height);
                levelListDrawable.setLevel(1);
                t.setText(t.getText()); // invalidate() doesn't work correctly...

                //

            } catch (Exception e) { /* Like a null bitmap, etc. */ }
        }
    }
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText("Kết quả");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }
    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
}
