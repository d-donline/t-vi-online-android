package com.android.tuvionline.model;

/**
 * Created by loidv on 8/23/16.
 */
public class Account {
    public String username;
    public String password;
    public String name;
    public String token;
    public String fullname;
    public Account(){}
    public Account(String user , String pass , String name , String token , String fullname) {
        this.username = user;
        this.password = pass;
        this.name = name;
        this.token = token;
        this.fullname = fullname;
    }
}
