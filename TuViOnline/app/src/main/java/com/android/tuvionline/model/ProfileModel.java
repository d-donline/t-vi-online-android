package com.android.tuvionline.model;

/**
 * Created by ABCD on 8/18/2016.
 */
public class ProfileModel {
    public String access_token;
    public String token_type;
    public String expires_in;
    public String userName;
    public String fullName;
}
