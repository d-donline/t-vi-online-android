package com.android.tuvionline;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.tuvionline.model.DataModel;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;


/**
 * Created by ABCD on 7/21/2016.
 */
public class ChiTietDiemBaoActivity extends AppCompatActivity {

    private TextView tvReuslt , tvTitle;
    private DataModel mData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tongquan);
        Gson gson = new Gson();
        mData = gson.fromJson(getIntent().getStringExtra("POST_DATA"), DataModel.class);
        initActionBar();
        init();
    }

    private void init() {
        tvReuslt = (TextView) findViewById(R.id.tvResult);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        if(mData != null){
            tvTitle.setText(mData.title);
             tvReuslt.setText(Html.fromHtml(mData.content));

        }

    }


    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText("Kết quả");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }
    private void backToHome(){
        Intent mIntent = new Intent(this , DiemBaoActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
}
