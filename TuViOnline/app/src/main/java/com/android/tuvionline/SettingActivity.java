package com.android.tuvionline;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tuvionline.common.AccountHolder;
import com.android.tuvionline.model.Account;
import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.model.TrangPhuc;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;

import java.util.Calendar;

import me.drakeet.materialdialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.widget.RelativeLayout.LayoutParams;

import org.w3c.dom.Text;

/**
 * Created by ABCD on 7/21/2016.
 */
public class SettingActivity extends AppCompatActivity {

    private ProgressDialog dialog;
    private ApiService.GitApiInterface service;
    private MaterialDialog mMaterialDialog;
    private NumberPicker mSpinerDate;
    private NumberPicker mHuongNha;
    private static final int MIN_YEAR = 1970;
    private static final int MAX_YEAR = 2099;
    private int currentYear;
    private int currentMonth;
    private RadioGroup radioGroup;
    private RadioButton radioSexButton;
    private EditText mFullname;
    private TextView tvDate;
    private Button mLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        service = ApiService.getClient();
        initActionBar();
        init();
    }

    private void init() {

        mFullname = (EditText) findViewById(R.id.edit_name);
        tvDate = (TextView) findViewById(R.id.tvDate);
        mLogout = (Button) findViewById(R.id.btn_logout);
        //check if login
        if(AccountHolder.getAccount(SettingActivity.this) != null){
            Account account = AccountHolder.getAccount(SettingActivity.this);
            mFullname.setText(account.fullname);
        }
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*DialogFragment newFragment = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        tvDate.setText("" + day + "/" + month+1 + "/" + year);
                    }
                };
                newFragment.show(getFragmentManager(), "datePicker");*/
            }
        });
        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AccountHolder.removeAccount(SettingActivity.this);
                Intent mIntent5 = new Intent(SettingActivity.this,MainActivity.class);
                DirectionUtils.changeActivity(SettingActivity.this,R.anim.slide_in_from_right,R.anim.slide_out_to_left,true,mIntent5);
            }
        });
    }


    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(false);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
       // ImageButton ibBack = (ImageButton) mCustomView.findViewById(R.id.ib_back);
        mTitleTextView.setText("Tử vi online");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
     /*   ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToHome();
            }
        });*/
    }


    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
   /* public  class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener{

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT,this,year,month,day);

            // Create a TextView programmatically.
            TextView tv = new TextView(getActivity());

            // Create a TextView programmatically
            LayoutParams lp = new RelativeLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, // Width of TextView
                    LayoutParams.WRAP_CONTENT); // Height of TextView
            tv.setLayoutParams(lp);
            tv.setPadding(10, 10, 10, 10);
            tv.setGravity(Gravity.CENTER);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
            tv.setText("This is a custom title.");
            tv.setTextColor(Color.parseColor("#ff0000"));
            tv.setBackgroundColor(Color.parseColor("#FFD2DAA7"));

            // Set the newly created TextView as a custom tile of DatePickerDialog
            //dpd.setCustomTitle(tv);

            // Or you can simply set a tile for DatePickerDialog
            *//*
                setTitle(CharSequence title)
                    Set the title text for this dialog's window.
            *//*
            dpd.setTitle("This is a simple title."); // Uncomment this line to activate it

            // Return the DatePickerDialog
            return  dpd;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){
            // Do something with the chosen date

        }
    }*/

}

