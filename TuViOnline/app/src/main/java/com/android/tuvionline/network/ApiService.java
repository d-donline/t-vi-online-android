package com.android.tuvionline.network;

import com.android.tuvionline.model.AccountPost;
import com.android.tuvionline.model.BoiToan;
import com.android.tuvionline.model.BoiVkCk;
import com.android.tuvionline.model.ChieuMenh;
import com.android.tuvionline.model.Contributor;
import com.android.tuvionline.model.DiemBao;
import com.android.tuvionline.model.LoginModel;
import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.model.ProfileModel;
import com.android.tuvionline.model.ResponseModel;
import com.android.tuvionline.model.SimSoDep;
import com.android.tuvionline.model.TrangPhuc;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ABCD on 7/5/2016.
 */
public class ApiService {
    private static GitApiInterface gitApiInterface ;
    private static String baseUrl  = "http://api.totravo.com/" ;

    public static GitApiInterface getClient() {
        if (gitApiInterface == null) {

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface ;
    }
    public interface GitApiInterface {
        /*-----------------------------------Method $GET API----------------------------------------------------*/
        @GET("api/repos/{owner}/{repo}/contributors")
        Call<List<Contributor>> getSearchList(@Path("owner") String owner, @Path("repo") String repo);

        @GET("api/Account/ExternalLogin")
        Call<ResponseModel> getLogin(@Path("username ") String username , @Path("password ") String password );

        @GET("api/phong-thuy/gioi-thieu/{repo}")
        Call<NetworkModel> getDataIntro(@Path("repo") String repo);


        @GET("api/giai-xam/xem-giai-mong")
        Call<NetworkModel> getGiaiMOngIntro(@Query("alphabet") String action);

        @GET("api/giai-xam/xem-giai-xam")
        Call<NetworkModel> getGiaiXamIntro();
        /*-----------------------------------Method $POST API----------------------------------------------------*/

        @POST("api/Account/Register")
        Call<String> registeUser(@Body AccountPost params);

        @POST("api/Account/ForgotPassword")
        Call<String> forgetUser(@Body AccountPost params);


        @FormUrlEncoded
        @POST("token")
        Call<ProfileModel> loginUser(@Field("grant_type") String grant_type , @Field("username") String username , @Field("password") String password);

        @POST("api/phong-thuy/xem-trang-phuc")
        Call<NetworkModel> getDataTrangPhuc(@Body TrangPhuc params);

        @POST("api/phong-thuy/xem-tuoi-xay-nha")
        Call<NetworkModel> getDataXayNha(@Body TrangPhuc params);

        @POST("api/phong-thuy/xem-huong-nha")
        Call<NetworkModel> getDataHuongNha(@Body TrangPhuc params);

        @POST("api/phong-thuy/xem-nam-sinh-con")
        Call<NetworkModel> getDataSinhCon(@Body BoiToan params);

        @POST("api/phong-thuy/xem-sao-chieu-menh")
        Call<NetworkModel> getDataChieumenh(@Body ChieuMenh params);

        @POST("api/phong-thuy/xem-not-ruoi")
        Call<NetworkModel> getDataNotRuoi(@Body ChieuMenh params);

        @POST("api/phong-thuy/xem-thoi-van")
        Call<NetworkModel> getDataThoiVan(@Body ChieuMenh params);

        @POST("api/phong-thuy/xem-boi-ai-cap")
        Call<NetworkModel> getDataAiCap(@Body BoiToan params);

        @POST("api/phong-thuy/xem-sim-so-dep")
        Call<NetworkModel> getDataSimSoDep(@Body SimSoDep params);

        @POST("api/phong-thuy/xem-ngay-tot-xau")
        Call<NetworkModel> getDataNgayTot(@Body BoiToan params);

        @POST("api/giai-xam/xem-diem-bao")
        Call<NetworkModel> getDataDiemBao(@Body DiemBao params);

        @POST("api/phong-thuy/xem-tuoi-vo-chong")
        Call<NetworkModel> getDataVkCk(@Body BoiVkCk params);
        /*-----------------------------------Method $PUT API----------------------------------------------------*/
    }
}
