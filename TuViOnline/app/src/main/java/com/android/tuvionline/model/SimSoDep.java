package com.android.tuvionline.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by loidv on 8/23/16.
 */
public class SimSoDep {
    //sinh con
    @SerializedName("sosim")
    String sosim;
    @SerializedName("ngaysinh")
    String ngaysinh;
    @SerializedName("thangsinh")
    String thangsinh;

    @SerializedName("namsinh")
    String namsinh;

    @SerializedName("gioitinh")
    String gioitinh;

    @SerializedName("giosinh")
    String giosinh;

    public SimSoDep(String val1 , String val2 , String val3 , String val4 , String val5 , String val6){
        this.sosim = val1;
        this.ngaysinh = val2;
        this.thangsinh = val3;
        this.namsinh = val4;
        this.gioitinh = val5;
        this.giosinh = val6;
    }


}

