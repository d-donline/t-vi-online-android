package com.android.tuvionline;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tuvionline.adapter.AdapterListCategory;
import com.android.tuvionline.model.BoiToan;
import com.android.tuvionline.model.DiemBao;
import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;

import me.drakeet.materialdialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ABCD on 7/21/2016.
 */
public class DiemBaoActivity extends AppCompatActivity {

    private ProgressDialog dialog;
    private ApiService.GitApiInterface service;
    private DatePicker mDatePicker;
    private NumberPicker mSaoChieu;
    private Button mDropTime , mDropDiem;
    private Button mButtonView;
    private  String[] HUONGS;
    private AdapterListCategory mAdapterCat;
    private ListView listViewItems;
    private AlertDialog alertDialogStores;
    private String[] cRaces;
    private String[] mDiemBao;

    private String[] cRacesValue;
    private String[] mDiemBaoValue;
    private String mTimeTag ="" , mDiemBaoTag = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diembao);
        service = ApiService.getClient();
        initActionBar();
        init();
    }

    private void init() {
        cRaces = getResources().getStringArray(R.array.planets_array);
        mDiemBao = getResources().getStringArray(R.array.diembao_array);

        cRacesValue = getResources().getStringArray(R.array.planets_array_values);
        mDiemBaoValue = getResources().getStringArray(R.array.diembao_array_values);

        mDropTime = (Button)findViewById(R.id.click_drop);
        mDropDiem = (Button) findViewById(R.id.click_diem_bao);
        mDropTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(cRaces);
            }
        });
        mDropDiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogDiemBao(mDiemBao);
            }
        });
        mButtonView = (Button) findViewById(R.id.btn_view);
        mButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTimeTag.equals("") || mDiemBaoTag.equals("")){
                    Toast.makeText(DiemBaoActivity.this , "Vui lòng chọn điều cần xem điềm báo!" , Toast.LENGTH_SHORT).show();
                }else{
                    loadData();
                }

            }
        });
    }

    private void showDialogDiemBao(String[] mDiemBao) {
        mAdapterCat = new AdapterListCategory(this, R.layout.list_view_row_item, mDiemBao , mDiemBaoValue);
        listViewItems = new ListView(this);
        listViewItems.setAdapter(mAdapterCat);

        listViewItems.setOnItemClickListener(new OnItemClickListenerListViewItemDiemBao());
        alertDialogStores = new AlertDialog.Builder(this)
                .setView(listViewItems)
                .show();
    }

    private void showDialog(String[] mData) {
        mAdapterCat = new AdapterListCategory(this, R.layout.list_view_row_item, mData , cRacesValue);
        listViewItems = new ListView(this);
        listViewItems.setAdapter(mAdapterCat);

        listViewItems.setOnItemClickListener(new OnItemClickListenerListViewItemCity());
        alertDialogStores = new AlertDialog.Builder(this)
                .setView(listViewItems)
                .show();
    }
    private class OnItemClickListenerListViewItemCity implements android.widget.AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            TextView textViewItem = ((TextView) view.findViewById(R.id.textViewItem));
            // get the clicked item name
            String listItemText = textViewItem.getText().toString();
            mTimeTag = textViewItem.getTag().toString();
            //int position = Integer.parseInt(listItemId);
            mDropTime.setText(listItemText);

            //LocalData.POST_SEARCH_CITY_ID = mDataLocation[position]._id;
            alertDialogStores.cancel();
        }
    }
    private class OnItemClickListenerListViewItemDiemBao implements android.widget.AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            TextView textViewItem = ((TextView) view.findViewById(R.id.textViewItem));
            // get the clicked item name
            String listItemText = textViewItem.getText().toString();
            mDiemBaoTag = textViewItem.getTag().toString();
            //int position = Integer.parseInt(listItemId);
            mDropDiem.setText(listItemText);

            //LocalData.POST_SEARCH_CITY_ID = mDataLocation[position]._id;
            alertDialogStores.cancel();
        }
    }
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText("Xem điềm báo");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
    }
    private void loadData() {
        DiemBao trangPhuc = new DiemBao(mTimeTag , mDiemBaoTag);
        dialog = ProgressDialog.show(DiemBaoActivity.this, "", "Đang tải dữ liệu...");
        Call<NetworkModel> call = service.getDataDiemBao(trangPhuc);
        call.enqueue(new Callback<NetworkModel>() {
            @Override
            public void onResponse(Call<NetworkModel> call, Response<NetworkModel> response) {
                if(response.body()== null){
                    dialog.dismiss();
                    Toast.makeText(DiemBaoActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
                }else{
                    if(response.body().data != null){
                        Intent intent = new Intent(DiemBaoActivity.this, ChiTietDiemBaoActivity.class);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(response.body().data);
                        intent.putExtra("POST_DATA", myJson);
                        startActivity(intent);
                    }
                    else{
                        dialog.dismiss();
                        Toast.makeText(DiemBaoActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
                    }
                }

                dialog.dismiss();
            }
            @Override
            public void onFailure(Call<NetworkModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(DiemBaoActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }

}
