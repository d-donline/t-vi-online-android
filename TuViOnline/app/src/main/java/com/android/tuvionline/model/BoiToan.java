package com.android.tuvionline.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by loidv on 8/23/16.
 */
public class BoiToan {
    //sinh con
    @SerializedName("nam_bo")
    String nam_bo;
    @SerializedName("nam_me")
    String nam_me;
    @SerializedName("nam_con")
    String nam_con;

    @SerializedName("nameguest")
    String nameguest;
    //
    @SerializedName("mDay")
    String mDay;
    @SerializedName("mMonth")
    String mMonth;
    @SerializedName("mYear")
    String mYear;
    @SerializedName("vieclam")
    String vieclam;

    public BoiToan(String val1 , String val2 , String val3){
        this.nam_bo = val1;
        this.nam_me = val2;
        this.nam_con = val3;
    }

    public BoiToan(String val1 ){
        this.nameguest = val1;

    }
    public BoiToan(String val1 , String val2 , String val3 , String val4){
        this.mDay = val1;
        this.mMonth = val2;
        this.mYear = val3;
        this.vieclam = val4;
    }
}

