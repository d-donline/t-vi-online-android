package com.android.tuvionline.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ABCD on 7/25/2016.
 */
public class TrangPhuc {
    @SerializedName("mDay")
    String mDay;
    @SerializedName("mMonth")
    String mMonth;
    @SerializedName("mYear")
    String mYear;
    @SerializedName("gioitinh")
    String gioitinh;

    @SerializedName("year_birth")
    String year_birth;

    @SerializedName("year_build")
    String year_build;

    @SerializedName("huongnha")
    String huongnha;


    public TrangPhuc(String mDay, String mMonth , String mYear, String gioitinh ) {
        this.mDay = mDay;
        this.mMonth = mMonth;
        this.mYear = mYear;
        this.gioitinh = gioitinh;
    }

    public TrangPhuc(String val1, String val2) {
        this.year_birth = val1;
        this.year_build = val2;
    }

    public TrangPhuc(String val1, String val2 , String val3) {
        this.year_birth = val1;
        this.huongnha = val2;
        this.gioitinh = val3;
    }


}
