package com.android.tuvionline.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.tuvionline.model.Account;

public class AccountHolder {
    private static final String KEY_DATA = "tuvi_account_holder";
    private static final String KEY_EMAIL = "tuvi_key_email";
    private static final String KEY_PASSWORD = "tuvi_key_password";
    private static final String KEY_NR_USER_NAME = "tuvi_key_user_name";
    private static final String KEY_NR_USER_TOKEN = "tuvi_key_token";
    private static final String KEY_NR_USER_FULLNAME = "tuvi_key_fullname";


    public static void saveAccount(Context context, Account account){
        if(account == null)
            return;
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_DATA, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_EMAIL, account.username).commit();
        sharedPreferences.edit().putString(KEY_PASSWORD, account.password).commit();
        sharedPreferences.edit().putString(KEY_NR_USER_NAME, account.name).commit();
        sharedPreferences.edit().putString(KEY_NR_USER_TOKEN, account.token).commit();
        sharedPreferences.edit().putString(KEY_NR_USER_FULLNAME, account.fullname).commit();
    }


    public static void removeAccount(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_DATA, Context.MODE_PRIVATE);
        sharedPreferences.edit().remove(KEY_EMAIL).commit();
        sharedPreferences.edit().remove(KEY_PASSWORD).commit();
        sharedPreferences.edit().remove(KEY_NR_USER_NAME).commit();
        sharedPreferences.edit().remove(KEY_NR_USER_TOKEN).commit();
        sharedPreferences.edit().remove(KEY_NR_USER_FULLNAME).commit();
    }

    public static Account getAccount(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_DATA, Context.MODE_PRIVATE);
        Account account = new Account();
        account.username = sharedPreferences.getString(KEY_EMAIL, "");
        if(account.username.length() == 0)
            return null;
        account.password = sharedPreferences.getString(KEY_PASSWORD, "");
        account.name = sharedPreferences.getString(KEY_NR_USER_NAME, "");
        account.token = sharedPreferences.getString(KEY_NR_USER_TOKEN, "");
        account.fullname = sharedPreferences.getString(KEY_NR_USER_FULLNAME, "");
        return account;
    }



}


