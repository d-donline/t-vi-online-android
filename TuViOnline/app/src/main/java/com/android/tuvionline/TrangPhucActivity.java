package com.android.tuvionline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.model.TrangPhuc;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import me.drakeet.materialdialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ABCD on 7/21/2016.
 */
public class TrangPhucActivity extends AppCompatActivity implements  DatePickerDialog.OnDateSetListener{

    private Button mButtonView;
    private ProgressDialog dialog;
    private ApiService.GitApiInterface service;
    private MaterialDialog mMaterialDialog;
    //private DatePicker mDatePicker;
    private RadioGroup radioGroup;
    private RadioButton radioSexButton;
    private Button dateTextView;
    private String mDay = "" , mMonth ="" , mYear ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trangphuc);
        service = ApiService.getClient();
        initActionBar();
        //setCurrentDateOnView();
        init();
    }

    private void init() {
        //mDatePicker = (DatePicker) findViewById(R.id.dp);
        mButtonView = (Button) findViewById(R.id.btn_view);
        mButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mDay.equals("")){
                    Toast.makeText(TrangPhucActivity.this , " Vui lòng chọn ngày sinh" , Toast.LENGTH_SHORT).show();
                }else{
                    loadData();
                }

            }
        });
        radioGroup = (RadioGroup) findViewById(R.id.radioSex);
        dateTextView = (Button)findViewById(R.id.date_textview);
        dateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        TrangPhucActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                //dpd.setThemeDark(modeDarkDate.isChecked());
                //dpd.vibrate(vibrateDate.isChecked());
               // dpd.dismissOnPause(dismissDate.isChecked());
                //dpd.showYearPickerFirst(showYearFirst.isChecked());
                dpd.setAccentColor(Color.parseColor("#8C3C31"));
                dpd.setTitle("DatePicker Title");


                dpd.show(getFragmentManager(), "Datepickerdialog");

            }
        });
    }

    private void loadData() {
        int selectedId = radioGroup.getCheckedRadioButtonId();
        radioSexButton = (RadioButton) findViewById(selectedId);
        TrangPhuc trangPhuc = new TrangPhuc(mDay,mMonth,mYear,radioSexButton.getText().toString());
        dialog = ProgressDialog.show(TrangPhucActivity.this, "", "Đang tải dữ liệu...");
        Call<NetworkModel> call = service.getDataTrangPhuc(trangPhuc);
        call.enqueue(new Callback<NetworkModel>() {
            @Override
            public void onResponse(Call<NetworkModel> call, Response<NetworkModel> response) {
                if (response.body().status == 200) {
                    Intent intent = new Intent(TrangPhucActivity.this, ChiTietNoHtmlActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(response.body().data);
                    intent.putExtra("POST_DATA", myJson);
                    startActivity(intent);
                }else{
                    Toast.makeText(TrangPhucActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
            @Override
            public void onFailure(Call<NetworkModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(TrangPhucActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText(getResources().getString(R.string.title_top_trang_phuc));

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);

    }
    // display current date
    public void setCurrentDateOnView() {

        Calendar c= Calendar.getInstance();
        int year = c.get(c.YEAR);
        int month = c.get(c.MONTH);
        int dayOfMonth = c.get(c.DAY_OF_MONTH);
        DatePicker dp = (DatePicker) findViewById(R.id.dp);
        dp.init(
                year,
                month,
                dayOfMonth,
                new DatePicker.OnDateChangedListener() {

                    @Override
                    public void onDateChanged(
                            DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth)
                    {
                        //Display the changed date to app interface
                       // tv.setText("Date changed [mm/dd/yyyy]:\n" + monthOfYear + "/" + dayOfMonth + "/" + year);
                    }
                });

    }
    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = ""+dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        dateTextView.setText(date);
        mDay = ""+dayOfMonth;
        mMonth = ""+monthOfYear;
        mYear = ""+ year;
    }
}
