package com.android.tuvionline.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.tuvionline.MainActivity;
import com.android.tuvionline.R;
import com.android.tuvionline.common.AccountHolder;
import com.android.tuvionline.model.Account;


public class OAuthFragment extends DialogFragment {
 
    private WebView webViewOauth;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
 
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //check if the login was successful and the access token returned
            //this test depend of your API
            if (url.contains("access_token=")) {
                //save your token
                saveAccessToken(url);
                return true;
            }
            //BaseActivity.logEvent(Consts.EVENT_CALLBACK + "Login Failed", true);
            return false;
        }
    }
 
    private void saveAccessToken(String url) {
        // extract the token if it exists
        String paths[] = url.split("access_token=");
        String username[] = url.split("external_user_name=");
        if (paths.length > 1) {

            Account account = new Account();
            account.username = Uri.decode(username[1]);
            account.password = "";
            account.name = Uri.decode(username[1]);
            account.token = paths[1];
            account.fullname = Uri.decode(username[1]);
            //save data
            AccountHolder.saveAccount(getActivity() , account);

            Intent intent2 = new Intent(getActivity(), MainActivity.class);
            intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent2);
            return;
        }
    }
 
    @Override
    public void onViewCreated(View arg0, Bundle arg1) {
        super.onViewCreated(arg0, arg1);
        //load the url of the oAuth login page
        webViewOauth
                .loadUrl("http://api.totravo.com/api/Account/ExternalLogin?provider=Facebook&response_type=token&client_id=HoroscopeApp&redirect_uri=http://api.zapto.org/authcomplete.html");
        //set the web client
        webViewOauth.setWebViewClient(new MyWebViewClient());
        //activates JavaScript (just in case)
        WebSettings webSettings = webViewOauth.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Retrieve the webview
        View v = inflater.inflate(R.layout.oauth_screen, container, false);
        webViewOauth = (WebView) v.findViewById(R.id.web_oauth);
        getDialog().setTitle("Login Facebook");
        return v;
    }
}
