package com.android.tuvionline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.tuvionline.model.ResponseModel;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private ApiService.GitApiInterface service;
    private ProgressDialog dialog;
    private EditText mUserName  , mPassword ;
    private Button mLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        initToolbar();
        init();
        event();
    }
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
    private void init() {
        service = ApiService.getClient();
        mUserName = (EditText) findViewById(R.id.edit_username);
        mPassword = (EditText) findViewById(R.id.edit_pass);
        mLogin = (Button) findViewById(R.id.button_login);
    }
    private void event() {
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidate();
            }
        });
    }
    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    private void checkValidate() {
        String rName = mUserName.getText().toString();
        String rUserPass = mPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(rName)) {
            mUserName.setError("UserName không được rỗng!");
            cancel = true;
            focusView = mUserName;
        }else if(TextUtils.isEmpty(rUserPass)) {
            mPassword.setError("Password không được rỗng!");
            cancel = true;
            focusView = mPassword;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            registerUser(rName  , rUserPass );
        }
    }

    private void registerUser(String rName,String rUserPassPre) {
        dialog = ProgressDialog.show(LoginActivity.this, "", "Đang đăng nhập...");
        Call<ResponseModel> call = service.getLogin(rName, rUserPassPre);
        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(LoginActivity.this , "Đăng nhập thành công" , Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this , "Đăng nhập không thành công" , Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(LoginActivity.this , "Đăng nhập không thành công" , Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
