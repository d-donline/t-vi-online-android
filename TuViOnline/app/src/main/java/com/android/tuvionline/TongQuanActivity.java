package com.android.tuvionline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tuvionline.adapter.MyPageAdapter;
import com.android.tuvionline.fragment.MyFragment;
import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ABCD on 7/21/2016.
 */
public class TongQuanActivity extends AppCompatActivity {

    private ProgressDialog dialog;
    private ApiService.GitApiInterface service;
    private TextView tvReuslt;
    MyPageAdapter pageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tongquan_2);
        service = ApiService.getClient();
        initActionBar();
        init();
    }

    private void init() {
        //tvReuslt = (TextView) findViewById(R.id.tvResult);
        loadData();

    }
    private List<Fragment> getFragments(String[] content){

        List<Fragment> fList = new ArrayList<Fragment>();

        for (int i = 0 ; i < content.length ; i++){
            fList.add(MyFragment.newInstance(content[i]));
        }
        return fList;

    }

    private void loadData() {
        dialog = ProgressDialog.show(TongQuanActivity.this, "", "Đang tải dữ liệu...");
        Call<NetworkModel> call = service.getDataIntro("tong-luan-phong-thuy");
        call.enqueue(new Callback<NetworkModel>() {
            @Override
            public void onResponse(Call<NetworkModel> call, Response<NetworkModel> response) {
                if (response.body().status == 200) {
                    String lines[] = response.body().data.content.split("\\.\\r?\\n");
                    //tvReuslt.setText(lines[1]);
                    List<Fragment> fragments = getFragments(lines);

                    pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);

                    ViewPager pager = (ViewPager)findViewById(R.id.viewpager);

                    pager.setAdapter(pageAdapter);
                }else{
                    Toast.makeText(TongQuanActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
            @Override
            public void onFailure(Call<NetworkModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(TongQuanActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText("Tổng luận phong thủy");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);

    }

    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }

        return super.onOptionsItemSelected(item);
    }

}
