package com.android.tuvionline;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.RelativeLayout;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.android.tuvionline.common.AccountHolder;
import com.android.tuvionline.model.Account;
import com.android.tuvionline.model.AccountPost;
import com.android.tuvionline.model.LoginModel;
import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.model.ProfileModel;
import com.android.tuvionline.model.ResponseModel;
import com.android.tuvionline.model.TrangPhuc;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.ui.OAuthFragment;
import com.android.tuvionline.ui.SnackbarUI;
import com.android.tuvionline.utils.DirectionUtils;

import com.google.gson.Gson;

import me.drakeet.materialdialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ApiService.GitApiInterface service;
    private ProgressDialog dialog;
    private ProgressDialog dialogRegister;
    private Button mBtnTuVi, mBtnBoiToan , mBtnTracnghiem, mBtnPhongThuy, mBtnGiaixam;
    public MaterialDialog mMaterialDialog;
    private PopupWindow mPopupWindow;
    private RelativeLayout mRelativeLayout;
    private ImageButton mLogin , mLoginFace , mRegiter;
    private View customView;
    private  LayoutInflater inflater;
    private Dialog dialogLogin; // class variable

    //Login
    private EditText mUsername;
    private  EditText mPassWord;
    private TextView mTvForget;
    private EditText mRegitserUser, mRegisterFull , mRegisterPass, mRegitserRePass;
    private LinearLayout mAreaLogin;
    private RelativeLayout mAreaProfile;
    private TextView mNameUser;
    private Button mMoney;

    //
    private Dialog dialogForget; // class variable
    private EditText mEmailForget;
    private MenuItem mSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initActionBar();
        init();
        service = ApiService.getClient();
    }

    private void init() {
        inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        mAreaLogin = (LinearLayout) findViewById(R.id.area_login);
        mAreaProfile = (RelativeLayout) findViewById(R.id.area_profile);
        mNameUser = (TextView) findViewById(R.id.tvName);
        mMoney = (Button) findViewById(R.id.money);

        mBtnTuVi = (Button) findViewById(R.id.btn_tuvi);
        mBtnBoiToan = (Button) findViewById(R.id.btn_boitoan);
        mBtnTracnghiem = (Button) findViewById(R.id.btn_tracnghiem);
        mBtnPhongThuy = (Button) findViewById(R.id.btn_phongthuy);
        mBtnGiaixam = (Button) findViewById(R.id.btn_giaixam);
        mBtnTracnghiem.setAlpha(0.5f);
        //
        DirectionUtils.buttonHorver(mBtnTuVi);
        DirectionUtils.buttonHorver(mBtnBoiToan);
        DirectionUtils.buttonHorver(mBtnPhongThuy);
        DirectionUtils.buttonHorver(mBtnGiaixam);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl);

        //
        mLogin = (ImageButton) findViewById(R.id.btn_login);
        mLoginFace = (ImageButton) findViewById(R.id.btn_face);
        mRegiter = (ImageButton) findViewById(R.id.btn_regiter);

        //event

        mLogin.setOnClickListener(myButtonListener);
        mLoginFace.setOnClickListener(myButtonListener);
        mRegiter.setOnClickListener(myButtonListener);
        //check if login
        if(AccountHolder.getAccount(MainActivity.this) != null){
            Account account = AccountHolder.getAccount(MainActivity.this);
            mNameUser.setText(account.fullname);
            mAreaLogin.setVisibility(View.GONE);
            mAreaProfile.setVisibility(View.VISIBLE);
        }
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
       // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        mTitleTextView.setText("Tử vi online");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public boolean onPrepareOptionsMenu(Menu menu)
    {

        MenuItem register = menu.findItem(R.id.action_settings);
        if(AccountHolder.getAccount(MainActivity.this) != null){
            register.setVisible(true);
        }else{
            register.setVisible(false);
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent mIntent5 = new Intent(this,SettingActivity.class);
            DirectionUtils.changeActivity(this,R.anim.slide_in_from_right,R.anim.slide_out_to_left,true,mIntent5);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(AccountHolder.getAccount(MainActivity.this) != null){
            switch (v.getId()){
                case R.id.btn_tuvi:
                    customView = inflater.inflate(R.layout.box_tu_vi,null);
                    showDialog(customView);
                    Button btn_trondoi =(Button)customView.findViewById(R.id.btn_tuvitrondoi);
                    Button btn_tuongtay =(Button)customView.findViewById(R.id.btn_tuongtay);
                    Button btn_laso =(Button)customView.findViewById(R.id.btn_laso);
                    Button btn_giaihan =(Button)customView.findViewById(R.id.btn_giaihan);
                    Button btn_tuvi =(Button)customView.findViewById(R.id.btn_tuvi2016);

                    btn_trondoi.setOnClickListener(myButtonListener);
                    btn_tuongtay.setOnClickListener(myButtonListener);
                    btn_laso.setOnClickListener(myButtonListener);
                    btn_giaihan.setOnClickListener(myButtonListener);
                    btn_tuvi.setOnClickListener(myButtonListener);
                    DirectionUtils.buttonHorver(btn_trondoi);
                    DirectionUtils.buttonHorver(btn_tuongtay);
                    DirectionUtils.buttonHorver(btn_laso);
                    DirectionUtils.buttonHorver(btn_giaihan);
                    DirectionUtils.buttonHorver(btn_tuvi);
                    break;
                case R.id.btn_boitoan:
                    customView = inflater.inflate(R.layout.box_boi_toan,null);
                    Button btnBoiVkCk =(Button)customView.findViewById(R.id.btn_boi_vk);
                    Button btnBoiSinhCon =(Button)customView.findViewById(R.id.btn_sinh_con);
                    Button btnSaoChieuMenh =(Button)customView.findViewById(R.id.btn_saochieu);
                    Button btnNutRuoi =(Button)customView.findViewById(R.id.btn_nutruoi);
                    Button btnThoiVan = (Button) customView.findViewById(R.id.btn_thoivan);
                    Button btnAiCap = (Button) customView.findViewById(R.id.btn_aicap);
                    Button btnSimSoDep = (Button) customView.findViewById(R.id.btn_simsodep);
                    Button btnNgayTotXau = (Button) customView.findViewById(R.id.btn_ngay_tot_xau);
                    Button btnTinhyeu = (Button) customView.findViewById(R.id.btn_tinh_yeu);

                    btnBoiVkCk.setOnClickListener(myButtonListener);
                    btnBoiSinhCon.setOnClickListener(myButtonListener);
                    btnSaoChieuMenh.setOnClickListener(myButtonListener);
                    btnNutRuoi.setOnClickListener(myButtonListener);
                    btnThoiVan.setOnClickListener(myButtonListener);
                    btnAiCap.setOnClickListener(myButtonListener);
                    btnSimSoDep.setOnClickListener(myButtonListener);
                    btnNgayTotXau.setOnClickListener(myButtonListener);
                    btnTinhyeu.setOnClickListener(myButtonListener);
                    //
                    DirectionUtils.buttonHorver(btnBoiVkCk);
                    DirectionUtils.buttonHorver(btnBoiSinhCon);
                    DirectionUtils.buttonHorver(btnSaoChieuMenh);
                    DirectionUtils.buttonHorver(btnNutRuoi);
                    DirectionUtils.buttonHorver(btnThoiVan);
                    DirectionUtils.buttonHorver(btnAiCap);
                    DirectionUtils.buttonHorver(btnSimSoDep);
                    DirectionUtils.buttonHorver(btnNgayTotXau);
                    DirectionUtils.buttonHorver(btnTinhyeu);
                    showDialog(customView);
                    break;
                case R.id.btn_giaixam:
                    customView = inflater.inflate(R.layout.box_giai_xam,null);
                    showDialog(customView);
                    Button btn_giaixam =(Button)customView.findViewById(R.id.btn_giaixam);
                    Button btn_diembao =(Button)customView.findViewById(R.id.btn_diembao);
                    Button btn_giaimong =(Button)customView.findViewById(R.id.btn_giaimong);
                    btn_giaixam.setOnClickListener(myButtonListener);
                    btn_diembao.setOnClickListener(myButtonListener);
                    btn_giaimong.setOnClickListener(myButtonListener);

                    DirectionUtils.buttonHorver(btn_giaixam);
                    DirectionUtils.buttonHorver(btn_diembao);
                    DirectionUtils.buttonHorver(btn_giaimong);


                    break;
                case R.id.btn_phongthuy:
                    customView = inflater.inflate(R.layout.box_main,null);
                    showDialog(customView);
                    //init
                    Button btn_huong_nha =(Button)customView.findViewById(R.id.btn_huong_nha);
                    Button btn_xaynha =(Button)customView.findViewById(R.id.btn_xaynha);
                    Button btn_tongluan =(Button)customView.findViewById(R.id.btn_tongluan);
                    Button btn_trangphuc =(Button)customView.findViewById(R.id.btn_trangphuc);
                    btn_huong_nha.setOnClickListener(myButtonListener);
                    btn_xaynha.setOnClickListener(myButtonListener);
                    btn_tongluan.setOnClickListener(myButtonListener);
                    btn_trangphuc.setOnClickListener(myButtonListener);

                    DirectionUtils.buttonHorver(btn_huong_nha);
                    DirectionUtils.buttonHorver(btn_xaynha);
                    DirectionUtils.buttonHorver(btn_tongluan);
                    DirectionUtils.buttonHorver(btn_trangphuc);

                    break;
                case R.id.btn_tracnghiem:
                    //Toast.makeText(this, "Chức năng đang hoàn thiện" , Toast.LENGTH_SHORT).show();
                    //Intent mIntent5 = new Intent(this,TracNghiemActivity.class);
                    //DirectionUtils.changeActivity(this,R.anim.slide_in_from_right,R.anim.slide_out_to_left,true,mIntent5);
                    break;

                default:
                    break;
            }
        }else{

            SnackbarUI.Snack(MainActivity.this ,getWindow().getDecorView().getRootView(),getString(R.string.bar_info),Snackbar.LENGTH_SHORT );
        }

    }
    private void showDialog(View customView){

        Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //customView.animate().rotationBy(360).r.setInterpolator(new AccelerateDecelerateInterpolator()).start();
        dialog.setContentView(customView);
       // dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        //Animation startRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.android_rotate_animation);
        //customView.startAnimation(startRotateAnimation);
        final RotateAnimation rotate = new RotateAnimation(-360, 360, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setFillAfter(true);
        rotate.setDuration(1000);
        final AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        alpha.setFillAfter(true);
        alpha.setDuration(1000);
        final ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setFillAfter(true);
        scale.setDuration(1000);
        final AnimationSet s = new AnimationSet(true);
        s.addAnimation(rotate);
        s.addAnimation(alpha);
        s.addAnimation(scale);
        customView.findViewById(R.id.menu_layout).startAnimation(s);
        dialog.show();
    }
    private void showDialogRegister(){
        dialogLogin = new Dialog(MainActivity.this);  // always give context of activity.
        dialogLogin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLogin.setContentView(R.layout.dialog_register);
        dialogLogin.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //init
        mRegitserUser = (EditText) dialogLogin.findViewById(R.id.regist_username);
        mRegisterFull = (EditText) dialogLogin.findViewById(R.id.regist_fullname);
        mRegisterPass = (EditText) dialogLogin.findViewById(R.id.regist_pass);
        mRegitserRePass = (EditText) dialogLogin.findViewById(R.id.regist_re_pass);
        ImageButton mRegister = (ImageButton) dialogLogin.findViewById(R.id.btn_register);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegister();
            }
        });
        ImageView dialogButton = (ImageView) dialogLogin.findViewById(R.id.iv_close);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialogLogin.dismiss();
            }
        });
        dialogLogin.setCanceledOnTouchOutside(true);
        dialogLogin.show();
    }
    private void showDialog()
    {
        dialogLogin = new Dialog(MainActivity.this);  // always give context of activity.
        dialogLogin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLogin.setContentView(R.layout.dialog_login);
        dialogLogin.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageButton mLogin = (ImageButton) dialogLogin.findViewById(R.id.btn_login);
        mUsername = (EditText)dialogLogin.findViewById(R.id.edit_username);
        mPassWord = (EditText)dialogLogin.findViewById(R.id.edit_pass);
        mTvForget = (TextView) dialogLogin.findViewById(R.id.tvForget);

        mLogin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                View view = MainActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                attemptLogin();
            }
        });
        mTvForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogin.dismiss();
                View view1 = MainActivity.this.getCurrentFocus();
                if (view1 != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                showDialogForget();
            }
        });
        ImageView dialogButton = (ImageView) dialogLogin.findViewById(R.id.iv_close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialogLogin.dismiss();
            }
        });
        dialogLogin.setCanceledOnTouchOutside(true);
        dialogLogin.show();
    }

    private void showDialogForget() {
        dialogForget = new Dialog(MainActivity.this);  // always give context of activity.
        dialogForget.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogForget.setContentView(R.layout.dialog_forget);
        dialogForget.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageButton mLogin = (ImageButton) dialogForget.findViewById(R.id.btn_login);
        mEmailForget = (EditText)dialogForget.findViewById(R.id.edit_username);

        mLogin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                View view = MainActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                attemptForget();
            }
        });

        ImageView dialogButton = (ImageView) dialogForget.findViewById(R.id.iv_close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialogForget.dismiss();
            }
        });
        dialogForget.setCanceledOnTouchOutside(true);
        dialogForget.show();
    }

    private void attemptForget() {
        mEmailForget.setError(null);
        String mEmail = mEmailForget.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if(TextUtils.isEmpty(mEmail)) {
            mEmailForget.setError(getString(R.string.error_field_required));
            focusView = mEmailForget;
            cancel = true;
        }else if(!isEmailValid(mEmail)){
            mEmailForget.setError(getString(R.string.error_field_required_email));
            focusView = mEmailForget;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            forgetEmail(mEmail);
        }
    }

    private void forgetEmail(String mEmail) {
        AccountPost accountPost = new AccountPost(mEmail);
        dialogRegister = ProgressDialog.show(MainActivity.this, "", "Đang gửi email...");
        Call<String> call = service.forgetUser(accountPost);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    dialogRegister.dismiss();
                    dialogForget.dismiss();
                    Toast.makeText(MainActivity.this , "Vui lòng xem Email để reset password!" , Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this , "Đăng ký không thành công" , Toast.LENGTH_SHORT).show();
                }
                dialogRegister.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialogRegister.dismiss();
                Toast.makeText(MainActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void attemptRegister() {
        // Reset errors.
        mRegitserUser.setError(null);
        mRegisterPass.setError(null);
        mRegitserRePass.setError(null);
        mRegisterFull.setError(null);
        // Store values at the time of the login attempt.
        String rName = mRegitserUser.getText().toString();
        String rUserPass = mRegisterPass.getText().toString();
        String rFullName = mRegisterFull.getText().toString();
        String rUserPassPre = mRegitserRePass.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid name address.
       if(TextUtils.isEmpty(rName)) {
            mRegitserUser.setError(getString(R.string.error_field_required));
            focusView = mRegitserUser;
            cancel = true;
        }else if(!isEmailValid(rName)){
           mRegitserUser.setError(getString(R.string.error_field_required_email));
           focusView = mRegitserUser;
           cancel = true;
       } else if(TextUtils.isEmpty(rFullName)){
           mRegisterFull.setError(getString(R.string.error_field_full_required));
           focusView = mRegisterFull;
           cancel = true;
       } else if(TextUtils.isEmpty(rUserPass)){
           mRegisterPass.setError(getString(R.string.error_field_pass_required));
            focusView = mRegisterPass;
            cancel = true;
        }else if (!TextUtils.isEmpty(rUserPass) && !isPasswordValid(rUserPass)) {
           mRegisterPass.setError(getString(R.string.error_invalid_password));
            focusView = mRegisterPass;
            cancel = true;
        }else if(!TextUtils.isEmpty(rUserPassPre) && !isPasswordValid(rUserPassPre)){
           mRegitserRePass.setError(getString(R.string.error_invalid_password));
            focusView = mRegitserRePass;
            cancel = true;
        }else if(!rUserPass.equals(rUserPassPre)){
           mRegitserRePass.setError(getString(R.string.error_invalid_password_re));
            focusView = mRegitserRePass;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            registerUser(rName , rFullName , rUserPass , rUserPassPre);
        }
    }
    private void registerUser(final String rName, String rFullName, String rUserPass, final String rUserPassPre) {
        AccountPost accountPost = new AccountPost(rName, rFullName,rUserPass,rUserPassPre );
        dialogRegister = ProgressDialog.show(MainActivity.this, "", "Đang đăng ký...");
        Call<String> call = service.registeUser(accountPost);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    dialogRegister.dismiss();
                    login(rName , rUserPassPre);


                }else{
                    Toast.makeText(MainActivity.this , "Đăng ký không thành công" , Toast.LENGTH_SHORT).show();
                }
                dialogRegister.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialogRegister.dismiss();
                Toast.makeText(MainActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void attemptLogin() {


        // Reset errors.
        mUsername.setError(null);
        mPassWord.setError(null);

        // Store values at the time of the login attempt.
        String email = mUsername.getText().toString();
        String password = mPassWord.getText().toString();

        boolean cancel = false;
        View focusView = null;



        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsername.setError(getString(R.string.error_field_required));
            focusView = mUsername;
            cancel = true;
        }else if(TextUtils.isEmpty(password)){
            mPassWord.setError(getString(R.string.error_field_pass_required));
            focusView = mPassWord;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            login(email,password);
        }
    }
    private void login(final String rName, final String rUserPassPre) {
        dialog = ProgressDialog.show(MainActivity.this, "", "Đang đăng nhập...");
        Call<ProfileModel> call = service.loginUser("password" , rName , rUserPassPre);
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                if (response.isSuccessful()) {
                    if(!response.body().access_token.equals("")){
                        mNameUser.setText(response.body().fullName);
                        mAreaLogin.setVisibility(View.GONE);
                        mAreaProfile.setVisibility(View.VISIBLE);
                        //
                        Account account = new Account();
                        account.username = rName;
                        account.password = rUserPassPre;
                        account.name = response.body().userName;
                        account.token = response.body().access_token;
                        account.fullname = response.body().fullName;
                        //save data
                        AccountHolder.saveAccount(MainActivity.this , account);
                        dialogLogin.dismiss();
                    }else{

                        Toast.makeText(MainActivity.this , "Đăng nhập không thành công" , Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(MainActivity.this , "Đăng nhập không thành công" , Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(MainActivity.this , "Đăng nhập không thành công" , Toast.LENGTH_SHORT).show();
            }
        });
    }
    private View.OnClickListener myButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_giaixam:
                    Intent mIntentGiaiXam = new Intent(MainActivity.this, GiaiXamActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentGiaiXam);
                    break;
                case R.id.btn_giaimong:
                    Intent mIntentGiaiMong = new Intent(MainActivity.this, GiaiMongActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentGiaiMong);
                    break;
                case R.id.btn_diembao:
                    Intent mIntentDiemBao = new Intent(MainActivity.this, DiemBaoActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentDiemBao);
                    break;
                case R.id.btn_huong_nha:
                    Intent mIntent = new Intent(MainActivity.this, HuongNhaActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntent);
                    break;
                case R.id.btn_xaynha:
                    Intent mIntent1 = new Intent(MainActivity.this, XayNhaActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntent1);
                    break;
                case R.id.btn_tongluan:
                    Intent mIntent4 = new Intent(MainActivity.this, TongQuanActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntent4);
                    break;
                case R.id.btn_trangphuc:
                    Intent mIntent3 = new Intent(MainActivity.this, TrangPhucActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntent3);
                    break;
                case R.id.btn_login:
                    showDialog();
                    break;
                case R.id.btn_regiter:
                    showDialogRegister();
                    break;
                case R.id.btn_face:
                    //showWebView();
                    showDialog1();
                    break;
                case R.id.btn_tuvi2016:
                    Intent mIntent2016 = new Intent(MainActivity.this, TuVi2016Activity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntent2016);
                    break;
                case R.id.btn_giaihan:
                    Intent mIntentGiaHan = new Intent(MainActivity.this, TuViGiaiHanActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentGiaHan);
                    break;
                case R.id.btn_laso:
                    Intent mIntentLaSo = new Intent(MainActivity.this, TuViLaSoActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentLaSo);
                    break;
                case R.id.btn_tuongtay:
                    Intent mIntentTuongtay = new Intent(MainActivity.this, TuongTayActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentTuongtay);
                    break;
                case R.id.btn_tuvitrondoi:
                    Intent mIntenttrondoi = new Intent(MainActivity.this, TuViTronDoiActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntenttrondoi);
                    break;
                case  R.id.btn_boi_vk:
                    Intent mIntentVk = new Intent(MainActivity.this, BoiVkCkActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentVk);
                    break;
                case R.id.btn_sinh_con:
                    Intent mIntentSinhCon = new Intent(MainActivity.this, SinhConActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentSinhCon);
                    break;
                case R.id.btn_saochieu:
                    Intent mIntentChieuMenh = new Intent(MainActivity.this, ChieuMenhActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentChieuMenh);
                    break;
                case R.id.btn_nutruoi:
                    Intent mIntentNutRuoi = new Intent(MainActivity.this, NutRuoiActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentNutRuoi);
                    break;
                case R.id.btn_thoivan:
                    Intent mIntentThoiVan = new Intent(MainActivity.this, BoiThoiVanActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentThoiVan);
                    break;
                case R.id.btn_aicap:
                    Intent mIntentAicap = new Intent(MainActivity.this, BoiAiCapActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentAicap);
                    break;
                case R.id.btn_simsodep:
                    Intent mIntentSimSo = new Intent(MainActivity.this, BoiSimSoDepActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentSimSo);
                    break;
                case R.id.btn_ngay_tot_xau:
                    Intent mIntentNgay = new Intent(MainActivity.this, BoiNgayTotXauActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentNgay);
                    break;
                case R.id.btn_tinh_yeu:
                    Intent mIntentTinhYeu = new Intent(MainActivity.this, BoiNgayTotXauActivity.class);
                    DirectionUtils.changeActivity(MainActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentTinhYeu);
                    break;

            }
        }
    };

    private void showWebView() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Đăng nhập bằng FaceBook");

        WebView wv = new WebView(this);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);

            }

        });
        wv.loadUrl("http://api.totravo.com/api/Account/ExternalLogin?provider=Facebook&response_type=token&client_id=HoroscopeApp&redirect_uri=http://api.zapto.org/authcomplete.html");

        alert.setView(wv);
        alert.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        alert.show();



    }
    void showDialog1() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(null);

        // Create and show the dialog.
        OAuthFragment newFragment = new OAuthFragment();
        newFragment.show(ft, "dialog");
    }


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 5;
    }
    private boolean isEmailValid(String email){
        return email.contains("@");
    }
    @Override
    public void onBackPressed() {
        MainActivity.this.finish();
    }
}
