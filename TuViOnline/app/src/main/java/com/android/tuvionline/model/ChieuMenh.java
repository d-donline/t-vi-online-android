package com.android.tuvionline.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by loidv on 8/23/16.
 */
public class ChieuMenh {
    //sinh con
    @SerializedName("namsinh")
    String namsinh;
    @SerializedName("gioitinh")
    String gioitinh;
    @SerializedName("namxem")
    String namxem;
   //
    @SerializedName("sonot")
    String sonot;
    // boi
    @SerializedName("mDay")
    String mDay;
    @SerializedName("mMonth")
    String mMonth;
    @SerializedName("mYear")
    String mYear;
    @SerializedName("testYear")
    String testYear;
    public ChieuMenh(String val1 , String val2 , String val3){
        this.namsinh = val1;
        this.gioitinh = val2;
        this.namxem = val3;
    }
    public ChieuMenh(String val1){
        this.sonot = val1;
    }
    public ChieuMenh(String val1 , String val2 , String val3 , String val4){
        this.mDay = val1;
        this.mMonth = val2;
        this.mYear = val3;
        this.testYear = val4;
    }
}
