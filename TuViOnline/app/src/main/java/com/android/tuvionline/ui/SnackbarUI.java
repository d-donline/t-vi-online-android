package com.android.tuvionline.ui;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.android.tuvionline.R;


/**
 * Created by ABCD on 5/10/2016.
 */
public class SnackbarUI {
    public static void Snack(Context context, View view , String text , int  duration){
        Snackbar snackbar = Snackbar.make(view, text, duration);

// set action button color
        snackbar.setActionTextColor(context.getResources().getColor(R.color.white));

// get snackbar view
        View snackbarView = snackbar.getView();

// change snackbar text color
        int snackbarTextId = android.support.design.R.id.snackbar_text;
        TextView textView = (TextView)snackbarView.findViewById(snackbarTextId);
        textView.setTextColor(context.getResources().getColor(R.color.black));

// change snackbar background
        snackbarView.setBackgroundColor(context.getResources().getColor(R.color.white));
        snackbar.show();
    }
}