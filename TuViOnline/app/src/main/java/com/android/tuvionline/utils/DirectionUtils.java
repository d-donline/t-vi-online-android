package com.android.tuvionline.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * 
 * DirectionUtils.java
 * 
 * @Author DoBao
 * @Email baodt@hanet.vn
 * @Phone +84983028786
 * @Skype baopfiev_k50
 * @Date Mar 14, 2014
 * @Project WhereMyLocation
 * @Package com.ypyproductions.utils
 * @Copyright � 2014 Softwares And Network Solutions HANET Co., Ltd
 */
public class DirectionUtils {
	
	public static void changeActivity(Activity mActivity, int animIn, int animOut,boolean hasFinish, Intent mIntent){
		if(mActivity==null||mIntent==null){
			return;
		}
		mActivity.startActivity(mIntent);
		mActivity.overridePendingTransition(animIn, animOut);
		if(hasFinish){
			mActivity.finish();
		}
	}
	public static void buttonHorver( final Button button){
		button.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
				if (arg1.getAction()==MotionEvent.ACTION_DOWN) {
					button.setAlpha(0.5f);
				}
				else if (arg1.getAction()==MotionEvent.ACTION_UP){
					button.setAlpha(1f);

				}
				return false;
			}
		});
	}
}
