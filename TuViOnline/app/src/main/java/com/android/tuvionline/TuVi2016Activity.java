package com.android.tuvionline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tuvionline.adapter.GridViewAdapter;
import com.android.tuvionline.model.Item;
import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.model.TrangPhuc;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;

import me.drakeet.materialdialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ABCD on 7/21/2016.
 */
public class TuVi2016Activity extends AppCompatActivity  implements AdapterView.OnItemClickListener {

    GridView gridview;
    GridViewAdapter gridviewAdapter;
    ArrayList<Item> data = new ArrayList<Item>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuvi2016);
        initActionBar();
        initView(); // Initialize the GUI Components
        fillData(); // Insert The Data
        setDataAdapter(); // Set the Data Adapter
    }

    // Initialize the GUI Components
    private void initView()
    {
        gridview = (GridView) findViewById(R.id.gridView);
        gridview.setOnItemClickListener(this);
    }

    // Insert The Data
    private void fillData()
    {
        data.add(new Item("Tý", ContextCompat.getDrawable(this,R.drawable.icon_ty)));
        data.add(new Item("Sửu", ContextCompat.getDrawable(this,R.drawable.icon_suu)));
        data.add(new Item("Dần", ContextCompat.getDrawable(this,R.drawable.icon_dau)));
        data.add(new Item("Mão", ContextCompat.getDrawable(this,R.drawable.icon_mao)));
        data.add(new Item("Thìn", ContextCompat.getDrawable(this,R.drawable.icon_thin)));
        data.add(new Item("Tỵ", ContextCompat.getDrawable(this,R.drawable.icon_ti)));
        data.add(new Item("Ngọ", ContextCompat.getDrawable(this,R.drawable.icon_ngo)));
        data.add(new Item("Mùi", ContextCompat.getDrawable(this,R.drawable.icon_mui)));
        data.add(new Item("Thân", ContextCompat.getDrawable(this,R.drawable.icon_than)));
        data.add(new Item("Dậu", ContextCompat.getDrawable(this,R.drawable.icon_ga)));
        data.add(new Item("Tuất", ContextCompat.getDrawable(this,R.drawable.icon_tuat)));
        data.add(new Item("Hợi", ContextCompat.getDrawable(this,R.drawable.icon_hoi)));
    }

    // Set the Data Adapter
    private void setDataAdapter()
    {
        gridviewAdapter = new GridViewAdapter(getApplicationContext(), R.layout.gird_items, data);
        gridview.setAdapter(gridviewAdapter);
    }

    @Override
    public void onItemClick(final AdapterView<?> arg0, final View view, final int position, final long id)
    {
        String message = "Clicked : " + data.get(position).getTitle();
        Toast.makeText(getApplicationContext(), message , Toast.LENGTH_SHORT).show();
    }


    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText("Tử vi 2016");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
    }
    // display current date

    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }


}
