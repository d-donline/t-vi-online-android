package com.android.tuvionline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tuvionline.model.BoiToan;
import com.android.tuvionline.model.ChieuMenh;
import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;

import java.util.Calendar;

import me.drakeet.materialdialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ABCD on 7/21/2016.
 */
public class BoiThoiVanActivity extends AppCompatActivity {

    private ProgressDialog dialog;
    private ApiService.GitApiInterface service;
    private MaterialDialog mMaterialDialog;
    private NumberPicker mSpinerDate;
    private DatePicker mDatePicker;
    private NumberPicker mSaoChieu;
    private static final int MIN_YEAR = 1970;
    private static final int MAX_YEAR = 2099;
    private int currentYear;
    private Button mButtonView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boithoivan);
        service = ApiService.getClient();
        initActionBar();
        init(2016);
    }

    private void init(int selectedYear) {
        mDatePicker = (DatePicker) findViewById(R.id.dp);
        final Calendar instance = Calendar.getInstance();
        currentYear = instance.get(Calendar.YEAR);

        if (selectedYear < MIN_YEAR || selectedYear > MAX_YEAR) {
            selectedYear = currentYear;
        }

        if (selectedYear == -1) {
            selectedYear = currentYear;
        }
        mSpinerDate = (NumberPicker) findViewById(R.id.spinner_sao_chieu);
        mSpinerDate.setMinValue(MIN_YEAR);
        mSpinerDate.setMaxValue(MAX_YEAR);
        mSpinerDate.setValue(selectedYear);
        mSpinerDate.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        mButtonView = (Button) findViewById(R.id.btn_view);
        mButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText("Xem thời vận");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
    }
    private void loadData() {
        ChieuMenh trangPhuc = new ChieuMenh(String.valueOf(mDatePicker.getDayOfMonth()),String.valueOf(mDatePicker.getMonth()),String.valueOf(mDatePicker.getYear()),String.valueOf(mSpinerDate.getValue()));
        dialog = ProgressDialog.show(BoiThoiVanActivity.this, "", "Đang tải dữ liệu...");
        Call<NetworkModel> call = service.getDataThoiVan(trangPhuc);
        call.enqueue(new Callback<NetworkModel>() {
            @Override
            public void onResponse(Call<NetworkModel> call, Response<NetworkModel> response) {
                if (response.body().status == 200) {
                    Intent intent = new Intent(BoiThoiVanActivity.this, ChiTietNoHtmlActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(response.body().data);
                    intent.putExtra("POST_DATA", myJson);
                    startActivity(intent);
                }else{
                    Toast.makeText(BoiThoiVanActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
            @Override
            public void onFailure(Call<NetworkModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(BoiThoiVanActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }

}
