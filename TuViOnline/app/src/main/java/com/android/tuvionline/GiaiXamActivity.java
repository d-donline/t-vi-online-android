package com.android.tuvionline;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.tuvionline.utils.DirectionUtils;
import com.android.tuvionline.utils.ShakeDetector;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ABCD on 7/21/2016.
 */
public class GiaiXamActivity extends AppCompatActivity  {
    private int shakes;
    private ImageView mImageCount;
    private ShakeDetector mShakeDetector;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giaixem);
        initToolbar();
        init();
    }

    private void init() {
        mImageCount = (ImageView) findViewById(R.id.ivView);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake() {
                // Do stuff!
                shakes++;
                if(shakes == 1){
                    mImageCount.setBackgroundResource(R.drawable.icon_1);
                }else if(shakes == 2){
                    mImageCount.setBackgroundResource(R.drawable.icon_2);
                }else if(shakes == 3){
                    mImageCount.setBackgroundResource(R.drawable.icon_3);
                }else if(shakes == 4){
                    mImageCount.setBackgroundResource(R.drawable.icon_4);
                }else if(shakes == 5){
                    mImageCount.setBackgroundResource(R.drawable.icon_5);
                }else if(shakes == 6){
                    mImageCount.setBackgroundResource(R.drawable.icon_6);
                }else if(shakes == 7){
                    mImageCount.setBackgroundResource(R.drawable.icon_7);
                }else if(shakes == 8){
                    mImageCount.setBackgroundResource(R.drawable.icon_8);
                }else if(shakes == 9){
                    mImageCount.setBackgroundResource(R.drawable.icon_9);
                }else if(shakes == 10){
                    mImageCount.setBackgroundResource(R.drawable.icon_10);
                }else if(shakes == 11){
                    mImageCount.setBackgroundResource(R.drawable.icon_11);
                }else if(shakes == 12){
                    mImageCount.setBackgroundResource(R.drawable.icon_12);
                }else if(shakes == 13){
                    mImageCount.setBackgroundResource(R.drawable.icon_13);
                }else if(shakes == 14){
                    mImageCount.setBackgroundResource(R.drawable.icon_14);
                }else if(shakes == 15){
                    mImageCount.setBackgroundResource(R.drawable.icon_15);
                }else if(shakes == 16){
                    mImageCount.setBackgroundResource(R.drawable.icon_16);
                }else if(shakes == 17){
                    mImageCount.setBackgroundResource(R.drawable.icon_17);
                    Intent mIntent = new Intent(GiaiXamActivity.this , ChiTietGiaiXamActivity.class);
                    DirectionUtils.changeActivity(GiaiXamActivity.this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
                }
                /*if (shakes > 3) {
                    Random r=new Random();
                    int i=r.nextInt(10);
                    tv.setText(""+i);
                }*/

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText("Giải xăm");

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }
    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }




}
