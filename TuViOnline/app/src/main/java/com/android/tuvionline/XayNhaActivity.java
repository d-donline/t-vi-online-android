package com.android.tuvionline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tuvionline.model.NetworkModel;
import com.android.tuvionline.model.TrangPhuc;
import com.android.tuvionline.network.ApiService;
import com.android.tuvionline.utils.DirectionUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;

import me.drakeet.materialdialog.MaterialDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ABCD on 7/21/2016.
 */
public class XayNhaActivity extends AppCompatActivity {
    private NumberPicker mSpinerDate;
    private NumberPicker mHuongNha;
    private ProgressDialog dialog;
    private ApiService.GitApiInterface service;
    private MaterialDialog mMaterialDialog;
    private Button mButtonView;
    private int currentYear;
    private static final int MIN_YEAR = 1970;
    private static final int MAX_YEAR = 2099;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xaynha);
        service = ApiService.getClient();
        initActionBar();
        init(2016);
    }

    private void init(int selectedYear) {
        mSpinerDate = (NumberPicker) findViewById(R.id.spinner_date);
        mHuongNha = (NumberPicker) findViewById(R.id.spinner_huong);
        final Calendar instance = Calendar.getInstance();
        currentYear = instance.get(Calendar.YEAR);

        if (selectedYear < MIN_YEAR || selectedYear > MAX_YEAR) {
            selectedYear = currentYear;
        }

        if (selectedYear == -1) {
            selectedYear = currentYear;
        }
        mSpinerDate.setMinValue(MIN_YEAR);
        mSpinerDate.setMaxValue(MAX_YEAR);
        mSpinerDate.setValue(selectedYear);
        mSpinerDate.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        //
        mHuongNha.setMinValue(MIN_YEAR);
        mHuongNha.setMaxValue(MAX_YEAR);
        mHuongNha.setValue(selectedYear + 1);
        mHuongNha.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        mButtonView = (Button) findViewById(R.id.btn_view);
        mButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }
    private void loadData() {
        TrangPhuc trangPhuc = new TrangPhuc(String.valueOf(mSpinerDate.getValue()), String.valueOf(mHuongNha.getValue()));
        dialog = ProgressDialog.show(XayNhaActivity.this, "", "Đang tải dữ liệu...");
        Call<NetworkModel> call = service.getDataXayNha(trangPhuc);
        call.enqueue(new Callback<NetworkModel>() {
            @Override
            public void onResponse(Call<NetworkModel> call, Response<NetworkModel> response) {
                if (response.body().status == 200) {
                    Intent intent = new Intent(XayNhaActivity.this, ChiTietNoHtmlActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(response.body().data);
                    intent.putExtra("POST_DATA", myJson);
                    startActivity(intent);
                }else{
                    Toast.makeText(XayNhaActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
            @Override
            public void onFailure(Call<NetworkModel> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(XayNhaActivity.this , "Lỗi kết nối dữ liệu" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        // set a custom icon for the default home button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_child, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);

        mTitleTextView.setText(getResources().getString(R.string.title_top_xay_nha));

        ab.setCustomView(mCustomView);
        ab.setDisplayShowCustomEnabled(true);

    }
    // display current date
    private void backToHome(){
        Intent mIntent = new Intent(this , MainActivity.class);
        DirectionUtils.changeActivity(this , R.anim.slide_in_from_right , R.anim.slide_out_to_left , true , mIntent);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            backToHome();
            return  true;
        }
        return super.onKeyDown(keyCode,event);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home){
            backToHome();
        }
        return super.onOptionsItemSelected(item);
    }

}
