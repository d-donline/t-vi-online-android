package com.android.tuvionline.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.tuvionline.R;
import com.android.tuvionline.model.RespondCatModel;

import java.util.ArrayList;

// here's our beautiful adapter
public class AdapterListCategory extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    String[] data = null;
    String[] dataValue = null;

    public AdapterListCategory(Context mContext, int layoutResourceId, String[] data , String[] dataValue) {
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
        this.dataValue = dataValue;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }
        TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);

        final String topic = data[position];
        textViewItem.setText(topic);
        textViewItem.setTag(dataValue[position]);
        return convertView;
         
    }

}