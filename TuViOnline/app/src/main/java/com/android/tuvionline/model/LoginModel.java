package com.android.tuvionline.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ABCD on 8/18/2016.
 */
public class LoginModel {
    @SerializedName("grant_type")
    String grant_type;
    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;


    public LoginModel(String grant_type, String username , String password ) {
        this.grant_type = grant_type;
        this.username = username;
        this.password = password;
    }
}
