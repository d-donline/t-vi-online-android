package com.android.tuvionline;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.android.tuvionline.utils.DirectionUtils;

public class SplashScreensActivity extends Activity {
	

	private static int SPLASH_TIME_OUT = 4000;
	private ImageView mLogo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar
		setContentView(R.layout.activity_splash_screen);
		mLogo = (ImageView) findViewById(R.id.logo);
		RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(Animation.INFINITE);
		anim.setDuration(2000);
		mLogo.startAnimation(anim);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				mLogo.setAnimation(null);
				Intent mIntentGiaiXam = new Intent(SplashScreensActivity.this, MainActivity.class);
				DirectionUtils.changeActivity(SplashScreensActivity.this, R.anim.slide_in_from_right, R.anim.slide_out_to_left, true, mIntentGiaiXam);
			}
		}, SPLASH_TIME_OUT);




	}
}
